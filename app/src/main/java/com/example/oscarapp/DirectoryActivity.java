package com.example.oscarapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DirectoryActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener{

    private static final String Request_TAG_LOGIN = "UserAutentication";
    private RequestQueue requestQueue;
    private RadioGroup rGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        requestQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
        String url = "https://dl.dropboxusercontent.com/s/4scnaqzioi3ivxc/diretor.json";

        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method.GET, url,
                new JSONObject(), this, this);
        jsonRequest.setTag("DirectorActivity");

        requestQueue.add(jsonRequest);

        rGroup = findViewById(R.id.directorRadioGroup);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(Request_TAG_LOGIN);
    }

    @Override
    public void onErrorResponse(VolleyError error){

        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResponse(Object response) {
        try {
            Bundle params = new Bundle();

            JSONObject objs = ((JSONObject) response);
            JSONArray array = objs.getJSONArray("diretor");
            ListView list;

            Integer[] id = new Integer[array.length()];

            String[] name = new String[array.length()];


            for (int i = 0; i < array.length(); i++) {
                JSONObject json = array.getJSONObject(i);
                name[i] = json.getString("nome");
                id[i] = json.getInt("id");
                RadioButton x = new RadioButton(this);
                x.setText(json.getString("nome"));
                rGroup.addView(x);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void votar(View view){
        Toast.makeText(this, "Diretor selecionado!", Toast.LENGTH_SHORT).show();
    }
}
