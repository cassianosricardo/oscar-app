package com.example.oscarapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.oscarapp.Director;

public class DirectorAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] name;
    private final Integer[] id;


    public DirectorAdapter(Activity context, String[] name, Integer[] id){
        super(context,R.layout.list_directors, name);

        this.context = context;
        this.name = name;
        this.id = id;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_directors, null, true);
        RadioButton txtTitle = (RadioButton) rowView.findViewById(R.id.radioDirector);

        txtTitle.setId(id[position]);
        txtTitle.setText(name[position]);

        return rowView;
    }

}
