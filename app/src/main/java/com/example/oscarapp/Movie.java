package com.example.oscarapp;


import java.io.Serializable;

public class Movie implements Serializable {
    private int id;
    private String name;
    private String genero;
    private String image;

    public Movie(int id, String name, String genero, String image) {
        this.id = id;
        this.name = name;
        this.genero = genero;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
