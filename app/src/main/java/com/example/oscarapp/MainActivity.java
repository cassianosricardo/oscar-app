package com.example.oscarapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener  {
    private Button bLogin;
    private EditText email;
    private static final String Request_TAG_LOGIN = "UserAutentication";
    private EditText password;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        bLogin = findViewById(R.id.login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        email.setText(intent.getStringExtra("login"));
        password.setText(intent.getStringExtra("password"));
    }

    public void createAccount(View view) {
        Bundle params = new Bundle();
        params.putString("login", email.getText().toString());
        params.putString("password", password.getText().toString());
        startActivity(new Intent(getApplicationContext(), CreateAccount.class).putExtras(params));
    }

    public void logar(View view){
        JSONObject user = new JSONObject();
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        if (email.getText().length() == 0 || password.getText().length() == 0){
            Toast.makeText(MainActivity.this, "Forneca um usuario e senha", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            user.put("email", email.getText().toString());
            user.put("password", password.getText().toString());

            requestQueue = CustomVolleyRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
            CustomJSONObjectRequest jsonObjectRequest = new CustomJSONObjectRequest(Request.Method.POST,
                    "http://192.168.1.148:3000/login",
                    user,
                    MainActivity.this, MainActivity.this);

            requestQueue.add(jsonObjectRequest);
        }catch (Exception e){
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(Request_TAG_LOGIN);
    }

    @Override
    public void onErrorResponse(VolleyError error){
        if (error.networkResponse.statusCode == 409){
            Toast.makeText(this, "Usuario/Senha invalidos", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(Object response) {
        try {
            Bundle params = new Bundle();
            params.putString("token", ((JSONObject) response).getString("token"));
            startActivity(new Intent(getApplicationContext(), Menu.class).putExtras(params));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
