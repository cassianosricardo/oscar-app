package com.example.oscarapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;

public class MoviesActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener  {
    private static final String Request_TAG_LOGIN = "UserAutentication";
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);


        requestQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
        String url = "https://dl.dropboxusercontent.com/s/luags6sv8uxdoj1/filme.json";

        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method.GET, url,
                new JSONObject(), this, this);
        jsonRequest.setTag("MoviesActivity");

        requestQueue.add(jsonRequest);

    }



    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null)
            requestQueue.cancelAll(Request_TAG_LOGIN);
    }

    @Override
    public void onErrorResponse(VolleyError error){

        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();

        //mTextView.setText(error.getMessage());
    }

    @Override
    public void onResponse(Object response) {
        try {

//            Toast.makeText(this, retu, Toast.LENGTH_SHORT).show();

            ListView list;

            JSONObject objs = ((JSONObject) response);

            JSONArray array = objs.getJSONArray("filme");


            Integer[] id = new Integer[array.length()];

            String[] name = new String[array.length()];

            String[] url = new String[array.length()];


            for (int i = 0; i < array.length(); i++) {
                JSONObject json = array.getJSONObject(i);
                name[i] = json.getString("nome");
                id[i] = json.getInt("id");
                url[i] = json.getString("foto");


                Toast.makeText(this, json.getString("nome"), Toast.LENGTH_SHORT).show();

            }


            MovieAdapter adapter = new MovieAdapter(MoviesActivity.this,name,url, id );

            list=(ListView)findViewById(R.id.listMovies);

            list.setAdapter(adapter);

            //startActivity(new Intent(getApplicationContext(), Menu.class).putExtras(params));
            //finish();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
