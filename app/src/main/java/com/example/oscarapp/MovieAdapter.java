package com.example.oscarapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class MovieAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] name;
    private final String[] image;
    private final Integer[] id;


    public MovieAdapter(Activity context, String[] name,String[] image, Integer[] id){
        super(context,R.layout.list_xml, name);

        this.image = image;
        this.context = context;
        this.name = name;
        this.id = id;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_xml, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.movieTitle);
        ImageView imagem = (ImageView) rowView.findViewById(R.id.movieImg);

        Picasso.get().load(image[position]).into(imagem);

        txtTitle.setText(name[position]);

        return rowView;
    }
}
