package com.example.oscarapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Menu extends AppCompatActivity {

    private TextView tokenView;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        setContentView(R.layout.activity_menu);
        tokenView = findViewById(R.id.token);
        token = intent.getStringExtra("token");
        tokenView.setText("Token: " + token);
    }

    public void logout(View view){
        finish();
    }

    public void vote_director(View view){
        startActivity(new Intent(getApplicationContext(), DirectoryActivity.class));
    }

    public void vote_movie(View view){
        startActivity(new Intent(getApplicationContext(), MoviesActivity.class));
    }

    public void confirm_vote(){
        Bundle params = new Bundle();
        params.putString("token", token);
        startActivity(new Intent(getApplicationContext(), Menu.class).putExtras(params));
    }
}
